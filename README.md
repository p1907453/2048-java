# 2048-Java

Implémentation Java en MVC du jeu 2048

Créé par Nathan Puricelli et Aymeric Leto

## Comment compiler le projet : 
    Pour compiler le projet, il faut avoir java JDK, qui fournit le programme javac pour compiler le code.
    -Compiler en ligne de commande : 
        -Depuis un terminal : $> javac -d bin src/modele/*.java src/vue-conteoleur/*.java src/*.java
    -Compiler avec ant :
        -Installer ant https://ant.apache.org/
        -Depuis un terminal : $> ant

## Comment executer le programme :
    Pour executer le programme, il faut déja l'avoir compilé.
    Ensuite , depuis un terminal : $> java -cp bin Main

## Comment générer la javadoc : 
    -Avec ant (https://ant.apache.org/) : 
        -Depuis un terminal : $> ant documentation
    -Sans ant :
        -Depuis un terminal à la racine du projet : $>javadoc -d doc - sourcepath src/*.java src/modele/*.java src/vue_controleur/*.java

Lien du projet sur la forge Lyon1 : https://forge.univ-lyon1.fr/p1907453/2048-java