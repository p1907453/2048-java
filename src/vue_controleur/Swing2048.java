package vue_controleur;

import modele.Case;
import modele.Direction;
import modele.Jeu;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

public class Swing2048 extends JFrame implements Observer {
    private static final int PIXEL_PER_SQUARE = 125;
    // tableau de cases : i, j -> case graphique
    private JLabel[][] tabC;
    private JLabel score;
    private JLabel maxScore;
    private Jeu jeu;
    private HashMap<Integer, Color> bgColorsMap;
    private HashMap<Integer, Color> txtColorsMap;
    private Font font;


    public Swing2048(Jeu _jeu) {
        jeu = _jeu;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("2048");
        setSize(1200, 800);
        setResizable(false);
        tabC = new JLabel[jeu.getSize()][jeu.getSize()];

        //Remplissage des couleurs.
        bgColorsMap = new HashMap<Integer, Color>();
        txtColorsMap = new HashMap<Integer, Color>();
        bgColorsMap.put(2, new Color(238, 228, 218));
        txtColorsMap.put(2, new Color(119, 110, 101));

        bgColorsMap.put(4, new Color(237, 224, 200));
        txtColorsMap.put(4, new Color(119, 110, 101));

        bgColorsMap.put(8, new Color(242, 177, 121));
        txtColorsMap.put(8, new Color(249, 246, 242));

        bgColorsMap.put(16, new Color(245, 149, 99));
        txtColorsMap.put(16, new Color(249, 246, 242));

        bgColorsMap.put(32, new Color(246, 124, 95));
        txtColorsMap.put(32, new Color(249, 246, 242));

        bgColorsMap.put(64, new Color(246, 94, 59));
        txtColorsMap.put(64, new Color(249, 246, 242));

        bgColorsMap.put(128, new Color(237, 207, 114));
        txtColorsMap.put(128, new Color(249, 246, 242));

        bgColorsMap.put(256, new Color(240, 224, 80));
        txtColorsMap.put(256, new Color(249, 246, 242));
        
        bgColorsMap.put(512, new Color(240, 224, 16));
        txtColorsMap.put(512, new Color(249, 246, 242));

        bgColorsMap.put(1024, new Color(240, 208, 0));
        txtColorsMap.put(1024, new Color(249, 246, 242));

        bgColorsMap.put(2048, new Color(240, 192, 0));
        txtColorsMap.put(2048, new Color(249, 246, 242));

        bgColorsMap.put(4096, new Color(0,0,255));
        txtColorsMap.put(4096, new Color(249, 246, 242));

        try{
        	font = Font.createFont(Font.TRUETYPE_FONT, new File("./assets/fonts/clear-sans.ttf"));
        } catch(Exception e){e.printStackTrace();}

        // Nord : Score + Score max + BTN retour
        JPanel haut = new JPanel();
        getContentPane().add(haut, BorderLayout.NORTH);
        GridBagLayout gridHaut = new GridBagLayout();
        gridHaut.columnWidths = new int[] {300, 100, 200, 150, 50, 300};
        gridHaut.rowHeights = new int[] {100};
        haut.setLayout(gridHaut);

        JLabel txt2048 = new JLabel("2048");
        txt2048.setFont(font.deriveFont(Font.BOLD, 36));
        txt2048.setHorizontalAlignment(SwingConstants.LEFT);
        GridBagConstraints txt2048Constraints = new GridBagConstraints();
        txt2048Constraints.insets = new Insets(10,10,10,10);
        txt2048Constraints.gridx = 1;
        txt2048Constraints.gridy = 0;
        haut.add(txt2048, txt2048Constraints);

        score = new JLabel("Score : 0");
        score.setFont(font.deriveFont(Font.BOLD, 30));
        score.setHorizontalAlignment(SwingConstants.LEFT);
        GridBagConstraints scoreConstraints = new GridBagConstraints();
        scoreConstraints.insets = new Insets(10,10,10,10);
        scoreConstraints.gridx = 2;
        scoreConstraints.gridy = 0;
        haut.add(score, scoreConstraints);

        maxScore = new JLabel("Max = 0");
        maxScore.setFont(font.deriveFont(Font.BOLD, 24));
        maxScore.setHorizontalAlignment(SwingConstants.LEFT);
        GridBagConstraints maxScoreConstraints = new GridBagConstraints();
        maxScoreConstraints.insets = new Insets(10,10,10,10);
        maxScoreConstraints.gridx = 3;
        maxScoreConstraints.gridy = 0;
        haut.add(maxScore, maxScoreConstraints);

        ImageIcon imageRetour = new ImageIcon("./assets/img/go-back-arrow.png");
        JButton retour = new JButton(imageRetour);
        retour.setFocusable(false);
        retour.setHorizontalAlignment(SwingConstants.LEFT);
        GridBagConstraints retourConstraints = new GridBagConstraints();
        retourConstraints.insets = new Insets(5,5,5,5);
        retourConstraints.gridx = 4;
        retourConstraints.gridy = 0;
        haut.add(retour, retourConstraints);

        retour.addActionListener(new ActionListener(){
	        public void actionPerformed(ActionEvent e){
	            jeu.retour();
	        }
	    });

        JPanel droite = new JPanel();
        getContentPane().add(droite, BorderLayout.EAST);

        JButton Sauvegarde = new JButton("Sauvegarder");
        Sauvegarde.setFocusable(false);
        Sauvegarde.setHorizontalAlignment(SwingConstants.CENTER);
        haut.add(Sauvegarde);

        Sauvegarde.addActionListener(new ActionListener(){
	        public void actionPerformed(ActionEvent e){
	            try {
                    jeu.Sauvegarde();
                    JOptionPane optionPane = new JOptionPane();
                    String options[] = {"OK"};
                    JOptionPane.showOptionDialog(optionPane, "La sauvegarde s'est bien effectuée", "Sauvegarde", 0, 1, null, options, options[0]);
            
                } catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                } catch (UnsupportedEncodingException e1) {
                    e1.printStackTrace();
                }
	        }
	    });



        JPanel contentPane = new JPanel();
        getContentPane().add(contentPane, BorderLayout.CENTER);
        GridBagLayout mainGrid = new GridBagLayout();
		contentPane.setLayout(mainGrid);

        //Contrainte de taille pour les cases : 
        for (int i = 0; i < jeu.getSize(); i++){
            for (int j = 0; j< jeu.getSize(); j++){
                GridBagConstraints constr = new GridBagConstraints();
                tabC[i][j] = new JLabel();
                tabC[i][j].setPreferredSize(new Dimension(PIXEL_PER_SQUARE, PIXEL_PER_SQUARE));
                constr.gridx = j;
                constr.gridy = i;
                Border border = BorderFactory.createLineBorder(Color.darkGray, 3);
                tabC[i][j].setBorder(border);
                tabC[i][j].setHorizontalAlignment(SwingConstants.CENTER);
                contentPane.add(tabC[i][j], constr);
            }
        }





        /* for (int i = 0; i < jeu.getSize(); i++) {
            for (int j = 0; j < jeu.getSize(); j++) {
                Border border = BorderFactory.createLineBorder(Color.darkGray, 3);
                tabC[i][j] = new JLabel();
                tabC[i][j].setBorder(border);
                tabC[i][j].setHorizontalAlignment(SwingConstants.CENTER);


                contentPane.add(tabC[i][j]);

            }
        } */
        //getContentPane().add(contentPane);
        //setContentPane(contentPane);
        ajouterEcouteurClavier();
        rafraichir();

    }




    /**
     * Correspond à la fonctionnalité de Vue : affiche les données du modèle
     */
    private void rafraichir()  {

        SwingUtilities.invokeLater(new Runnable() { // demande au processus graphique de réaliser le traitement
            @Override
            public void run() {
                for (int i = 0; i < jeu.getSize(); i++) {
                    for (int j = 0; j < jeu.getSize(); j++) {
                        Case c = jeu.getCase(i, j);
                        tabC[i][j].setOpaque(true);

                        if (c == null) {
                            tabC[i][j].setOpaque(false);
                            tabC[i][j].setText("");

                        } else {
                            tabC[i][j].setText(c.getValeur() + "");
                            tabC[i][j].setBackground(bgColorsMap.get(c.getValeur()));
                            tabC[i][j].setForeground(txtColorsMap.get(c.getValeur()));
                            tabC[i][j].setFont(font.deriveFont(Font.BOLD,PIXEL_PER_SQUARE/5));
                            
                        }


                    }
                }
                score.setText("Score : " + jeu.getScore());
                maxScore.setText("Max : " + jeu.getMaxScore());

                if (jeu.getPartieTerminee()) {
                    JOptionPane optionPane = new JOptionPane();
                    String options[] = {"Recommencer", "Quitter"};
                    int optionChoisie = optionPane.showOptionDialog(optionPane, "Vous avez perdu. Score : " + jeu.getScore(), "Défaite", 0, 0, null, options, options[0]);
                    if (optionChoisie == -1 || optionChoisie == 1) { //L'utilisateur à cliqué sur la croix, ou sur quitter :
                        // setVisible(false);
                        // dispose();
                        System.exit(0);
                    } else {
                        recommencer();
                    }
                }

            }
        });


    }

    /**
     * Correspond à la fonctionnalité de Contrôleur : écoute les évènements, et déclenche des traitements sur le modèle
     */
    private void ajouterEcouteurClavier() {
        addKeyListener(new KeyAdapter() { // new KeyAdapter() { ... } est une instance de classe anonyme, il s'agit d'un objet qui correspond au controleur dans MVC
            @Override
            public void keyPressed(KeyEvent e) {
                switch(e.getKeyCode()) {  // on regarde quelle touche a été pressée
                    case KeyEvent.VK_LEFT : jeu.move(Direction.gauche); break;
                    case KeyEvent.VK_RIGHT : jeu.move(Direction.droite); break;
                    case KeyEvent.VK_DOWN : jeu.move(Direction.bas); break;
                    case KeyEvent.VK_UP : jeu.move(Direction.haut); break;
                }
            }
        });
    }


    @Override
    public void update(Observable o, Object arg) {
        rafraichir();
    }

    public void recommencer() {
        jeu.reset();
        rafraichir();
    }
}