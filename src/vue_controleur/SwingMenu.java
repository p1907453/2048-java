package vue_controleur;

import modele.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.Observable;
import java.util.Observer;

public class SwingMenu extends JFrame implements Observer {
    private Font font;

    /**
     * Constructeur de la fenetre de menu
     */
    public SwingMenu() {
        //Création de la fenetre
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("2048");
        setSize(1200, 800);
        setResizable(false);

        //Création de la font
        try{
        	font = Font.createFont(Font.TRUETYPE_FONT, new File("./assets/fonts/clear-sans.ttf"));
        } catch(Exception e){e.printStackTrace();}

        //Création du panel menu
        JPanel panelMenu = new JPanel();
        getContentPane().add(panelMenu, BorderLayout.CENTER);
        GridBagLayout gridPanelMenu = new GridBagLayout();
        gridPanelMenu.columnWidths = new int[] {400};
        gridPanelMenu.rowHeights = new int[] {100, 150, 150, 150, 150, 100};
        panelMenu.setLayout(gridPanelMenu);

        //Création du texte 2048 
        JLabel txtTitre = new JLabel("2048");
        txtTitre.setFont(font.deriveFont(Font.BOLD, 120));
        GridBagConstraints txtTitreConstraints = new GridBagConstraints();
        txtTitreConstraints.insets = new Insets(10,10,10,10);
        txtTitreConstraints.gridx = 0;
        txtTitreConstraints.gridy = 1;
        panelMenu.add(txtTitre, txtTitreConstraints);


        //Création du bouton jouer
        JButton boutonJouer = new JButton("Jouer");
        boutonJouer.setFont(font.deriveFont(Font.BOLD, 52));
        GridBagConstraints boutonJouerConstraints = new GridBagConstraints();
        boutonJouerConstraints.insets = new Insets(10,10,10,10);
        boutonJouerConstraints.gridx = 0;
        boutonJouerConstraints.gridy = 2;
        panelMenu.add(boutonJouer, boutonJouerConstraints);

        //Création du bouton charger
        JButton boutonCharger = new JButton("Charger");
        boutonCharger.setFont(font.deriveFont(Font.BOLD, 52));
        GridBagConstraints boutonChargerConstraints = new GridBagConstraints();
        boutonChargerConstraints.insets = new Insets(10,10,10,10);
        boutonChargerConstraints.gridx = 0;
        boutonChargerConstraints.gridy = 3;
        panelMenu.add(boutonCharger, boutonChargerConstraints);


        //Création du bouton quitter
        JButton boutonQuitter = new JButton("Quitter");
        boutonQuitter.setFont(font.deriveFont(Font.BOLD, 52));
        GridBagConstraints boutonQuitterConstraints = new GridBagConstraints();
        boutonQuitterConstraints.insets = new Insets(10,10,10,10);
        boutonQuitterConstraints.gridx = 0;
        boutonQuitterConstraints.gridy = 4;
        panelMenu.add(boutonQuitter, boutonQuitterConstraints);

        //Définition de la fonction du bouton jouer
        boutonJouer.addActionListener(new ActionListener() {
	        public void actionPerformed(ActionEvent e) {
                Jeu jeu = new Jeu(4);
                Swing2048 vue = new Swing2048(jeu);
                jeu.addObserver(vue);
        
                setVisible(false);
                vue.setVisible(true);
	        }
	    });

        //Définition de la fonction du bouton charger
        boutonCharger.addActionListener(new ActionListener() {
	        public void actionPerformed(ActionEvent e) {
                Jeu jeu = new Jeu(4, "./assets/save/sauvegarde.txt");
                Swing2048 vue = new Swing2048(jeu);
                jeu.addObserver(vue);
        
                setVisible(false);
                vue.setVisible(true);
	        }
	    });


        //Définition de la fonction du bouton quitter
        boutonQuitter.addActionListener(new ActionListener(){
	        public void actionPerformed(ActionEvent e){
                System.exit(0);
	        }
	    });
    }

    @Override
    public void update(Observable o, Object arg) {
    }
}
