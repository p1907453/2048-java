import modele.Jeu;
import vue_controleur.*;
/**
 * Classe principale du projet, lance le jeu.
 * @author Nathan Puricelli et Aymeric Leto
 */
public class Main {

    public static void main(String[] args) {
        //mainConsole();
        mainSwing();

    }

    /**
     * Appelle le jeu en mode console
     */
    public static void mainConsole() {
        Jeu jeu = new Jeu(4);
        Console2048 vue = new Console2048(jeu);
        jeu.addObserver(vue);

        vue.start();

    }

    /**
     * Appelle le jeu en mode graphique
     */
    public static void mainSwing() {
        SwingMenu menu = new SwingMenu();
        menu.setVisible(true);
    }



}
