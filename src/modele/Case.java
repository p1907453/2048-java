package modele;

public class Case {
    ///Valeur de la case
    private int valeur;

    ///Instance du jeu dans laquelle la case se trouve
    private Jeu j;

    /**
     * Constructeur de la case
     * @param _valeur Valeur de la case
     * @param jeu Instance du jeu
     */
    public Case(int _valeur, Jeu jeu) {
        this.valeur = _valeur;
        this.j = jeu; 
    }

    /**
     * acesseur de la valeur
     * @return valeur de la case
     */
    public int getValeur() {
        return valeur;
    }

    /**Multiplie la valeur de la case par 2 */
    public void multiply(){
        this.valeur *= 2;
    }

    /**
     * Déplacement d'une case dans le jeu
     * @param D Direction du déplacement
     * @return Un booléen mis à vrai si la case a bougé ou a fusionné.
     */
    public boolean move(Direction D){
        boolean moved = false;
        Case v = j.getVoisin(D, this);
        if (v == null){
            moved = true;
            this.j.movecase(D, this);
        }
        else if(v.getValeur() == this.getValeur()){
            moved = true;
            this.j.merge(D, this);
        }
        return moved;
        
    }
    

}
