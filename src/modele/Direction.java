package modele;

/**Les 4 directions possibles du jeu. */
public enum Direction {
    haut, bas, gauche, droite
}
