package modele;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Observable;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Scanner;


/**
 * Classe gérant le jeu.
 */
public class Jeu extends Observable {
    /**Tableau de cases */ 
    private Case[][] tabCases;

    /**Tableau de cases au tour d'avant */ 
    private Case[][] tabCasesPrecedent;

    /**Association entre les cases et leurs coordonnées. */
    private ConcurrentHashMap<Case, Point> map;


    /**Objet random utilisé pour les générations. */
    private static Random rnd = new Random(1299292991);

    private int score, maxScore, scorePrecedent;
    private boolean partieTerminee;

    private int nbCoupsJoues;

    /**
     * Constructeur du jeu
     * @param size Taille de la grille de jeu
     */
    public Jeu(int size) {
        this.score = 0;
        this.scorePrecedent = 0;
        this.maxScore = 0;
        this.tabCases = new Case[size][size];
        rnd();
        //this.map = new HashMap<Case, Point>();
        this.map = new ConcurrentHashMap<Case, Point>();
        this.tabCasesPrecedent = new Case[size][size];
        this.partieTerminee = false;
        this.nbCoupsJoues = 0;
    }

    /**
     * Constructeur de jeu avec le fichier de sauvegarde
     * @param size Taille de la grille
     * @param filepath Fichier de sauvegarde
     */
    public Jeu(int size, String filepath) {
        this.tabCases = new Case[size][size];
        // rnd();
        this.map = new ConcurrentHashMap<Case, Point>();
        this.tabCasesPrecedent = new Case[size][size];
        this.partieTerminee = false;
        this.nbCoupsJoues = 0;

        try {
            File f = new File(filepath);
            Scanner scanner = new Scanner(f);
            int val;
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                    val = Integer.parseInt(scanner.next());
                    if (val != 0) {
                        this.tabCases[i][j] = new Case(val, this);
                        map.put(this.tabCases[i][j], new Point(i, j));
                    } else {
                        this.tabCases[i][j] = null;
                    }
                }
                scanner.nextLine();
            }
            setChanged();
            notifyObservers();
            this.score = Integer.parseInt(scanner.next());
            this.scorePrecedent = this.score;
            this.maxScore = Integer.parseInt(scanner.next());
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }


    /**
     * Retourne la taille du tableau
     * @return la taille du tableau de jeu.
     */
    public int getSize() {
        return this.tabCases.length;
    }

    /**
     * Retourne le score
     * @return le score
     */
    public int getScore(){
        return this.score;
    }

    /**
     * Retourne le score max
     * @return le score max
     */
    public int getMaxScore(){
        return this.maxScore;
    }

    /**
     * Retourne vrai si la partie est terminée
     * @return L'état de la partie
     */
    public boolean getPartieTerminee() {
        return this.partieTerminee;
    }

    /**
     * Donne la case aux coordonnées i j
     * @param i ordonnée de la case
     * @param j abscisse de la case
     * @return valeur de la case aux coordonnées i j
     */
    public Case getCase(int i, int j) {
        return this.tabCases[i][j];
    }

    /**
     * Retourne le voisin à une case selon la direction. Retourne une case de valeur -1 si le voisin est hors grille.
     * @param D Direction de la recherche
     * @param c Case pour qui rechercher le voisin
     * @return la case voisine, ou null si case vide, ou une case de valeur -1 si la case initiale est au bord.
     */
    public Case getVoisin(Direction D, Case c) {
        int x, y;
        Point coordPoint = map.get(c);
        switch (D) {
            case gauche:
                x = coordPoint.getX();
                y = coordPoint.getY() - 1;
                break;
            case droite:
                x = coordPoint.getX();
                y = coordPoint.getY() + 1;
                break;
            case bas:
                x = coordPoint.getX() + 1;
                y = coordPoint.getY();
                break;
            case haut:
                x = coordPoint.getX() - 1;
                y = coordPoint.getY();
                break;
            default:
                x = -1;
                y = -1;
                break;
        }
        if (x >= getSize() || x < 0 || y >= getSize() || y < 0) {
            return new Case(-1, this);
        }

        return this.tabCases[x][y];
    }

    /**
     * Sauvegarde le jeu dans un fichier texte
     * @throws FileNotFoundException
     * @throws UnsupportedEncodingException
     */
    public void Sauvegarde() throws FileNotFoundException, UnsupportedEncodingException{
        File f = new File("./assets/save/sauvegarde.txt");
        PrintWriter writer = new PrintWriter(f, "UTF-8");
        for(int i = 0; i<getSize(); i++){
            for(int j = 0; j< getSize(); j++){
                if(this.tabCases[i][j] != null){
                    writer.print(tabCases[i][j].getValeur());
                }
                else{
                    writer.print("0");
                }
                writer.print(" ");
            }
            writer.println();
        }
        writer.println(score);
        writer.println(maxScore);
        writer.close();
    }

    /**
     * Retourne le jeu à l'état précedent
     */
    public void retour(){
        if (nbCoupsJoues != 0) {
            nbCoupsJoues--;
            this.map.clear();
            
            for(int i = 0; i < getSize(); i++){
                for(int j = 0; j < getSize(); j++){
                    this.tabCases[i][j] = this.tabCasesPrecedent[i][j];
                    if(this.tabCases[i][j] != null){
                        this.map.put(this.tabCases[i][j], new Point(i, j));        
                    }        
                }
            }

            score = scorePrecedent;
                    
            setChanged();
            notifyObservers();
        }
    }

    /**
     * Déplace le jeu selon une direction D
     * @param D Direction du déplacement
     */
    public void move(Direction D) {
        scorePrecedent = score;
        boolean moved = false;
        Case[][] caseTemp = new Case[getSize()][getSize()];
        for(int i = 0; i < getSize(); i++){
            for(int j = 0; j< getSize(); j++){
                if(this.tabCases[i][j] != null){
                    caseTemp[i][j] = new Case(this.tabCases[i][j].getValeur(), this);
                }
                else{
                    caseTemp[i][j] = null;
                }
            }
        }

        switch (D) {
            case gauche:
                moved = this.gauche();
                break;
            case droite:
                moved = this.droite();
                break;
            case haut:
                moved = this.haut();
                break;
            case bas:
                moved = this.bas();
                break;
        }
        if (moved){
            this.newItem();
            for(int i = 0; i < getSize(); i++){
                for(int j = 0; j < getSize(); j++){
                    this.tabCasesPrecedent[i][j] = caseTemp[i][j];
                }
            }
            partieTerminee = this.verifPartieTerminee();
            nbCoupsJoues++;
        }
    }

    /**
     * Créé une nouvelle case à un emplacement vide.
     */
    private void newItem(){
        if(map.size() == this.getSize() * this.getSize()){
            System.out.println("Perdu nulos");
        }
        else{
            int x, y;
            do{
                x = rnd.nextInt(this.getSize());
                y = rnd.nextInt(this.getSize());
            }
            while(this.tabCases[x][y] != null);
            int value = (rnd.nextInt(2) + 1) * 2;
            Case c = new Case(value, this);
            tabCases[x][y] = c;
            map.put(c, new Point(x, y));
        }
    }

    /**
     * Déplace une case selon une direction
     * @param D Direction du déplacement
     * @param c Case à déplacer
     */
    public void movecase(Direction D, Case c) {
        Point coord = map.get(c);
        switch (D) {
            case gauche:
                this.tabCases[coord.getX()][coord.getY() - 1] = c;
                this.tabCases[coord.getX()][coord.getY()] = null;
                map.replace(c, coord, new Point(coord.getX(), coord.getY() - 1));
                if (coord.getY() > 1) {
                    c.move(D);
                }
                break;

            case droite:
                this.tabCases[coord.getX()][coord.getY() + 1] = c;
                this.tabCases[coord.getX()][coord.getY()] = null;
                map.replace(c, coord, new Point(coord.getX(), coord.getY() + 1));
                if (coord.getY() < 2) {
                    c.move(D);
                }
                break;

            case bas:
                this.tabCases[coord.getX() + 1][coord.getY()] = c;
                this.tabCases[coord.getX()][coord.getY()] = null;
                map.replace(c, coord, new Point(coord.getX() + 1, coord.getY()));
                if (coord.getX() < 2) {
                    c.move(D);
                }
                break;

            case haut:
                this.tabCases[coord.getX() - 1][coord.getY()] = c;
                this.tabCases[coord.getX()][coord.getY()] = null;
                map.replace(c, coord, new Point(coord.getX() - 1, coord.getY()));
                if (coord.getX() > 1) {
                    c.move(D);
                }
                break;
        }
    }

    /**
     * Fusionne une case selon une direction
     * @param D Direction de la fusion
     * @param c Case à fusionner
     */
    public void merge(Direction D, Case c) {
        Point coord = map.get(c);
        switch(D){
            case gauche:
                merge2cases(this.tabCases[coord.getX()][coord.getY()-1], c);
                break;
            case droite:
                merge2cases(this.tabCases[coord.getX()][coord.getY()+1], c);
                break;
            case haut:
                merge2cases(this.tabCases[coord.getX()-1][coord.getY()], c);
                break;
            case bas:
                merge2cases(this.tabCases[coord.getX()+1][coord.getY()], c);
                break;
        }
    }

    /**
     * Fusionne 2 cases
     * @param cible Case finale
     * @param source Case initiale
     */
    private void merge2cases(Case cible, Case source){
        Point coordSource = map.get(source);
        cible.multiply();
        this.score += cible.getValeur();
        if (maxScore < score) maxScore = score;
        this.tabCases[coordSource.getX()][coordSource.getY()] = null;
        map.remove(source);
    }

    /**
     * Déplace le jeu à gauche
     * @return Vrai si le jeu a bougé ou fusionné après l'appel.
     */
    private boolean gauche() {
        boolean moved = false;
        int i = 1;
        while (i < this.getSize() ) {
            for (Case c : map.keySet()) {
                if (map.get(c).getY() == i)
                    if(c.move(Direction.gauche)){moved = true;}
            }
            i++;
        }
        setChanged();
        notifyObservers();
        return moved;
    }

    /**
     * Déplace le jeu à droite
     * @return Vrai si le jeu a bougé ou fusionné après l'appel.
     */
    private boolean droite() {
        boolean moved = false;
        int i = this.getSize() - 2;
        while (i >= 0) {
            for (Case c : map.keySet()) {
                if (map.get(c).getY() == i)
                    if(c.move(Direction.droite)){moved = true;}
            }
            i--;
        }
        setChanged();
        notifyObservers();
        return moved;
    }

    /**
     * Déplace le jeu vers le haut
     * @return Vrai si le jeu a bougé ou fusionné après l'appel.
     */
    private boolean haut() {
        boolean moved = false;
        int i = 1;
        while (i < this.getSize() ) {
            for (Case c : map.keySet()) {
                if (map.get(c).getX() == i)
                    if(c.move(Direction.haut)){moved = true;}
            }
            i++;
        }
        setChanged();
        notifyObservers();
        return moved;
    }

    /**
     * Déplace le jeu vers le bas
     * @return Vrai si le jeu a bougé ou fusionné après l'appel.
     */
    private boolean bas() {
        boolean moved = false;
        int i = this.getSize() - 2;
        while (i >= 0 ) {
            for (Case c : map.keySet()) {
                if (map.get(c).getX() == i)
                    if(c.move(Direction.bas)){moved = true;}
            }
            i--;
        }
        setChanged();
        notifyObservers();
        return moved;
    }

    /**
     * Vérifie la perte de la partie
     * @return Vrai si la partie est finie.
     */
    private boolean verifPartieTerminee() {
        for (int i = 0; i < getSize(); i++) {
            for (int j = 0; j < getSize(); j++) {
                if (tabCases[i][j] == null) {
                    return false;
                }
                //Vérification qu'il n'y ai pas deux cases identiques à coté à l'horizontal :
                else if ((j <= getSize() - 2) && tabCases[i][j+1] != null && (tabCases[i][j].getValeur() == tabCases[i][j+1].getValeur())) {
                    return false;
                }
            }
        }
        //Même vérification, à la vertical cette fois ci
        for (int j = 0; j < getSize(); j++) {
            for (int i = 0; i < getSize() - 1; i++) {
                if (tabCases[i][j].getValeur() == tabCases[i + 1][j].getValeur())
                    return false;
            }
        }
        return true;
    }

    /** 
     * Créée une instance aléatoire de la grille.
     */
    public void rnd() {
        new Thread() { // permet de libérer le processus graphique ou de la console
            public void run() {
                int r;
                Case c;

                for (int i = 0; i < getSize(); i++) {
                    for (int j = 0; j < getSize(); j++) {
                        r = rnd.nextInt(10);

                        switch (r) {
                            case 1:
                                c = new Case(2, Jeu.this);
                                tabCases[i][j] = c;
                                map.put(c, new Point(i, j));
                                break;
                            case 2:
                                c = new Case(4, Jeu.this);
                                map.put(c, new Point(i, j));
                                tabCases[i][j] = c;
                                break;
                            default:
                                tabCases[i][j] = null;
                                break;
                        }
                        // c = new Case(2048,Jeu.this);
                        // tabCases[i][j] = c;
                        // map.put(c, new Point(i, j));
                    }   
                }
            }

        }.start();

        setChanged();
        notifyObservers();

    }

    /**
     * Reset la grille avec un nouveau jeu pour commencer.
     */
    public void reset() {
        this.score = 0;
        this.scorePrecedent = 0;
        this.tabCases = new Case[getSize()][getSize()];
        rnd();
        //this.map = new HashMap<Case, Point>();
        this.map = new ConcurrentHashMap<Case, Point>();
        this.tabCasesPrecedent = new Case[getSize()][getSize()];
        this.partieTerminee = false;
        this.nbCoupsJoues = 0;
    }

}
